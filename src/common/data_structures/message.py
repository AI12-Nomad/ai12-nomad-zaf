import uuid as uuid_lib
from profile import Player


class Message:
    """
    Data class for containing text messages

    At init : for a random message_uuid, don't set a uuid value, or set it at 0/false

    """

    def __init__(
        self,
        game_uuid: uuid_lib.UUID,
        player: Player,
        message: str,
        timestamp: int,
        message_uuid: uuid_lib.UUID = uuid_lib.uuid4(),
    ):
        self.game_uuid = game_uuid
        self.player = player
        self.message = message
        self.timestamp = timestamp

    def __repr__(self):
        return (
            f"message {self.uuid}"
            f"player: {self.player.nickname}"
            f"payload : {self.message}"
        )

    def __str__(self):
        return (
            f"message {self.uuid}\n"
            f"player: {self.player.uuid}\n"
            f"payload : {self.message}"
        )


if __name__ == "__main__":
    u = uuid_lib.uuid4()
    print(u)

    player = Player("Jean Michel le parpaing", u)

    message = Message(u, player, "coucou", 234567, uuid_lib.uuid4())
    print(f"p is {message}")
