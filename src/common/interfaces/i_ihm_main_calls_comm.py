from abc import ABC, abstractmethod
from uuid import UUID
from profile import Profile
from data_structures.local_game import LocalGame


class I_IHMMainCallsComm(ABC):
    @abstractmethod
    def remove_game(self, game_id: UUID):
        pass

    @abstractmethod
    def get_profile_info(self, user_id: UUID):
        pass

    @abstractmethod
    def create_game(self, local_game: LocalGame):
        pass

    @abstractmethod
    def update_profile(self, profil: Profile):
        pass

    @abstractmethod
    def spectate_game(self, user_id: UUID, game_id: UUID):
        pass

    @abstractmethod
    def join_game(self, user_id: UUID, game_id: UUID):
        pass

    @abstractmethod
    def connect_to_server(self, ip: str, port: int):
        pass

    @abstractmethod
    def disconnect_server(self):
        pass
